import cv2
import os
import sys
import time
import math
import hand_evaluator as he

file_path = os.path.dirname(os.path.abspath(__file__))

class CardAnalyzer:
	def __init__(self):
		table_card_templates_path = os.path.join(file_path, "card_images", "table_cards")
		self.table_cards_list = []
		for card in os.listdir(table_card_templates_path):
			template_image = cv2.imread(os.path.join(table_card_templates_path,card))
			if template_image is not None: 
				self.table_cards_list.append((card.split(".")[0], template_image))
		self.table_template_width = 18
		self.table_template_height = 40
		self.table_tm_buffer = 15
		self.table_first_card_position = (285,191)
		self.table_card_spacing = 51
	
                self.default_table_size = (803, 553)	
		self.hand_cards_list = []
                self.hand_orig_template_width =16
                self.hand_orig_template_height=34
		self.hand_template_width = 46
                self.hand_template_height = 54
		self.hand_tm_buffer = 5
		self.cards = [None, None]
		self.your_position = None

		for card in self.table_cards_list:
			self.hand_cards_list.append((card[0], cv2.resize(card[1], (self.hand_orig_template_width, self.hand_orig_template_height))))
		self.screenshot_dir = os.path.join(file_path, "screenshots")
		self.hidden_cards = cv2.imread(os.path.join(file_path, "hidden_cards.png"))
                self.top_row_amount= 22
                self.get_screenshot()
                h,w = self.screenshot.shape[:2]
                player_count = 9
                six_player_angles = [20, 90, 160, 200, 270, 340]
                nine_player_angles = [0, 35, 80, 120, 160, -160, -120, -80, -35]
                if player_count == 6: angles = six_player_angles
                if player_count == 9: angles = nine_player_angles
                radius_x = 320
                radius_y = 190
                self.center_table_y_offset = -80
                self.center_table_x_offset = -10
                self.player_data = {}
                for i in range(player_count):
                        player_name = "Player {}".format(i+1)
                        self.player_data[player_name] = {}
                        self.player_data[player_name]["in"] = False
                        
                        x_loc = int(w/2 + self.center_table_x_offset + radius_x*math.sin(math.radians(angles[i])))
                        if i == 0 and player_count%2: i = 1
                        y_loc = int(h/2 + self.center_table_y_offset +radius_y*math.cos(math.radians(angles[i])))
                        self.player_data[player_name]["location"] = (x_loc, y_loc)
                         
		for i in range(5):
			cv2.namedWindow("cropped_card_{}".format(i))

	def get_screenshot(self):
		os.system("python3 ~/Documents/code/window_id/pyscreencapture/screencapture.py OddsPoker -t '' -f ~/Documents/code/screenshots/screenshot.png")
		time.sleep(0.1)
		screenshot_images = os.listdir(self.screenshot_dir)
		self.screenshot = cv2.imread(os.path.join(self.screenshot_dir, screenshot_images[-1])) if len(screenshot_images) else None
		if self.screenshot is not None:
                        self.screenshot = self.screenshot[self.top_row_amount:,:]
                        self.screenshot = cv2.resize(self.screenshot, self.default_table_size)
                        os.remove(os.path.join(self.screenshot_dir, screenshot_images[-1]))
			return True
		return False

	def detect_table_cards(self):
		found_cards = []
		for card_index in range(5):
	 		cropped_card = self.screenshot[self.table_first_card_position[1]-self.table_tm_buffer:self.table_tm_buffer+self.table_first_card_position[1]+self.table_template_height, self.table_first_card_position[0]-self.table_tm_buffer+card_index*self.table_card_spacing:self.table_tm_buffer+self.table_first_card_position[0]+self.table_template_width+card_index*self.table_card_spacing]
			cv2.imshow("cropped_card_{}".format(card_index), cropped_card)
			cv2.waitKey(1)
			card = self.find_card(cropped_card)
			found_cards.append(card)		
		return found_cards

	def find_card(self, card_image, type="table"):
		if type == "table": cards_list = self.table_cards_list
		else: cards_list = self.hand_cards_list
                screenshot = self.screenshot.copy()
		for template in cards_list:
                	match_result = cv2.matchTemplate(card_image, template[1], cv2.TM_CCOEFF_NORMED)
                	mini, maxi, min_loc, max_loc = cv2.minMaxLoc(match_result)
                        if maxi > 0.93: return template[0]
        	return None

	def count_players(self):
		for player in self.player_data.values():
			player['in'] = False
		local_screenshot = self.screenshot.copy()
		self.players_in = 0
		cards_detected = True
		while cards_detected:
			match_result = cv2.matchTemplate(local_screenshot, self.hidden_cards, cv2.TM_CCOEFF_NORMED)
			mini, maxi, min_loc, max_loc = cv2.minMaxLoc(match_result)
			if maxi > 0.85:
				self.players_in += 1
				cv2.rectangle(local_screenshot, max_loc, (max_loc[0] + self.hidden_cards.shape[1], max_loc[1] + self.hidden_cards.shape[0]), [0,255,0], -1)
				for player in self.player_data.values():
					if (max_loc[0] + self.hand_tm_buffer, max_loc[1]+self.hand_tm_buffer) > player['location'] and (max_loc[0] - self.hand_tm_buffer, max_loc[1] - self.hand_tm_buffer) < player['location']:
						player['in'] = True
			else: return self.players_in

	def get_hand(self):
		cards = []
		screenshot = self.screenshot.copy()
		for player in self.player_data:
			pt1 = (self.player_data[player]['location'][0]+self.hand_tm_buffer, self.player_data[player]['location'][1]-self.hand_tm_buffer)
                        pt2 = (self.player_data[player]['location'][0]-self.hand_tm_buffer-self.hand_template_width, self.player_data[player]['location'][1]+self.hand_tm_buffer+self.hand_template_height)
                        pt3 = (self.player_data[player]['location'][0]-self.hand_tm_buffer, self.player_data[player]['location'][1]-self.hand_tm_buffer)
                        pt4 = (self.player_data[player]['location'][0]+self.hand_tm_buffer+self.hand_template_width, self.player_data[player]['location'][1]+self.hand_tm_buffer+self.hand_template_height)

			cv2.rectangle(screenshot, (pt1), (pt2), (0,255,0), 1)
                        cv2.rectangle(screenshot, (pt3), (pt4), (0,0,255),1)
                        cv2.putText(screenshot, player, pt1, cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255), 1)
                        cv2.namedWindow("hand positions")
                        cv2.imshow("hand positions", screenshot)
 
                        if self.player_data[player]["in"]: continue
                        cropped_screenshot = self.screenshot[pt1[1]:pt2[1], pt2[0]:pt1[0]]
                        found_card = self.find_card(cropped_screenshot, type="hand")
			if found_card != None:
				self.your_position = player
				cards.append(found_card)
				second_card = self.screenshot[pt3[1]:pt4[1], pt3[0]:pt4[0]]
				second_found_card = self.find_card(second_card, type="hand")
				cards.append(second_found_card)
				return cards
		
		return [None, None]

	def flop_turned(self, table_cards):
		for card in table_cards:
			if card is not None: return True
		return False

        def get_bets_training(self, cropped_image=None):
		b,g,r = cv2.split(self.screenshot)
                roi_w = 70
                roi_h = 25
                for player_name, player in self.player_data.items():
                        if not player['in']: continue
                        cv2.rectangle(g, player['bet_location'], (player['bet_location'][0]+roi_w, player['bet_location'][1]+roi_h),255)
                        bet_roi = g[player['bet_location'][1]:player['bet_location'][1]+roi_h, player['bet_location'][0]:player['bet_location'][0]+roi_w]
                        ret, threshed = cv2.threshold(bet_roi, 200, 255, cv2.THRESH_BINARY)
                        mean_val =cv2.mean(threshed)
                        print "mean val for player {} is {}".format(player_name, mean_val)
                        contours, hierarchy = cv2.findContours(threshed, cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
                        for cnt in contours:
                                [x,y,w,h] = cv2.boundingRect(cnt)
                                roi = threshed[y:y+h, x:x+w]
                                cv2.imshow("Found Number", roi)
                                number = raw_input("What number is this for {}?: ".format(player_name))
                                if number == "": continue
                                save_directory = sys.path.join(file_path, "ocr_training_data", "bets", number)
                                file_index = len(os.listdir(save_directory))
                                file_name = "{}-{}.jpg".format(number,file_index)
                                save_path = os.path.join(save_directory, file_name)
                                cv2.imwrite(save_path, roi)
                                
                cv2.imshow("green frame", g)
                        
		
if __name__ == '__main__' :
	ca = CardAnalyzer()
        cards_when_reported = None
	while True:
		valid_screenshot = ca.get_screenshot()
		if valid_screenshot:
			table_cards = ca.detect_table_cards()
			table_cards_length  = 5 - table_cards.count(None)
                        players_in = ca.count_players()
			if not ca.flop_turned(table_cards) and not ca.cards[0] and not ca.cards[1]:
				ca.cards = ca.get_hand()
	        		print "Your cards: ", ca.cards
                        oc = he.OutsCounter(ca.cards, table_cards)
                        if table_cards_length != cards_when_reported:
			        print "Table Cards:", table_cards
        			print "Your Position", ca.your_position
			        print "Players:", players_in
                                oc.GenerateReport()
                                cards_when_reported = table_cards_length
                        
			# if there are no players in, reset the hand
			if not ca.players_in: ca.cards = [None, None]

		else: time.sleep(0.5)
