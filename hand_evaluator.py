import itertools

class HandEvaluator():
	def __init__(self, cards):
                self.cards = [card for card in cards if card.name is not None]
                self.hand = [ card if card.name is not None else None for card in cards]
                self.hand_order = [('Straight Flush', self.isStraightFlush), ('Quads', self.isQuads), ('Full House',self.isFullHouse), ('Flush',self.isFlush), ('Straight',self.isStraight), ('Three of a Kind',self.isSet), ('Two Pairs',self.isTwoPair), ('Pair',self.isPair), ('High Card',self.isHighCard)]
                self.possible_hand_functions = [('Straight Flush', self.makeStraightFlush), ('Quads', self.makeQuads), ('Full House',self.makeFullHouse), ('Flush',self.makeFlush), ('Straight', self.makeStraight), ('Three of a Kind',self.makeSet), ('Two Pairs',self.makeTwoPair), ('Pair',self.makePair), ('High Card',self.isHighCard)]
 
                self.deck = Card().all_cards()
                self.card_names = [card.name for card in self.cards]
                self.remaining_cards = [card for card in self.deck if card.name not in self.card_names]
                self.cards_left = self.hand.count(None)
                low_values = ['2','3','4','5', 'ace']
                card_values = [card.value for card in self.cards]
                self.low_ace = True
                self.visible_cards = self.cards[:]
                # if any of the known cards aren't in the low values, we aren't dealing with a low ace straight
                for card in card_values:
                        if card not in low_values and card is not None: self.low_ace = False

        def removeKnownCards(self, cards):
                for card in cards:
                        self.remaining_cards = [rc for rc in self.remaining_cards if rc.name != card.name]
                        if card.name != None: self.visible_cards += [card]

        def returnData(self, value):
                _, high_card = self.isHighCard(self.cards)
                return value, high_card

        def isHighCard(self, cards=None):
                if cards is None: cards = self.cards
                all_values = Card().all_values 
                card_indecies = [all_values.index(card.value) for card in self.cards]
                max_value = max(card_indecies)
                return True, all_values[max_value]
 
        def _isMatched(self, cards, count):
                values = [card.value for card in cards]
                for value in values:
                        value_count = values.count(value)
                        if value_count == count: return True, value
                return False, None

        def _makeMatched(self, cards, count, original_count = None):
                reference_cards = []
                if not original_count: original_count = count
                # if we already have matched cards, we don't need any outs
                ismatched, _ = self._isMatched(cards, count)
                if ismatched: return True
                
                # if we don't have enough cards, we can't do it
                if original_count-count == self.cards_left: return False
 
                # if we want a pair, match all of the cards
                if count == 2:
                        valid_cards = []
                        for card in cards:
                                reference_value = card.value
                                valid_cards += [pair for pair in self.remaining_cards if pair.value == reference_value]
                        return valid_cards
                else:
                        found_group, reference_value =  self._isMatched(cards, count-1)
                        
                        if not found_group: return self._makeMatched(cards, count-1, original_count)                        
                        
                        return [card for card in self.remaining_cards if card.value == reference_value]
        
        def isPair(self, cards=None):
                if cards is None: cards = self.cards
                return self._isMatched(cards, 2)
        
        def makePair(self,cards = None):
                if cards is None: cards = self.cards
                return self._makeMatched(cards, 2)

        def isSet(self, cards=None):
                if cards is None: cards = self.cards
                return self._isMatched(cards, 3)

        def makeSet(self, cards=None):
                if cards is None: cards = self.cards
                return self._makeMatched(cards, 3)

        def isQuads(self, cards=None):
                if cards is None: cards = self.cards
                return self._isMatched(cards, 4)

        def makeQuads(self, cards=None):
                if cards is None: cards = self.cards
                return self._makeMatched(cards, 4)
        
        def isTwoPair(self):
                one_pair, first_pair = self.isPair()
                if not one_pair: return self.returnData(False)
                next_cards = [card for card in self.cards if card.value != first_pair]
                two_pair, second_pair = self.isPair(next_cards)
                if not two_pair: return False, first_pair
                high_card = max(first_pair, second_pair)
                return True, high_card

        def makeTwoPair(self):
                ispair, first_pair = self.isPair()
                # if we dont currently have a pair and we have at least two cards left
                if not ispair and self.cards_left > 2: return self.makePair()
                elif self.cards_left <2: return False
                next_cards = [card for card in self.cards if card.value != first_pair]
                return self.makePair(next_cards)
                
        def isFlush(self):
                if not self.complete(): return self.returnData(False)
                suits = [card.suit for card in self.cards]
                return self.returnData(suits.count(suits[0]) == len(suits))

        def makeFlush(self):
                isflush, _ = self.isFlush()
                if isflush: return True
                suit = self.cards[0].suit
                test_suits = [card.suit==suit for card in self.cards]
                if False in test_suits: return False
                return [ card for card in self.remaining_cards if card.suit == suit]

        def isStraight(self):
                if not self.complete(): return self.returnData(False)
                skipped_card_count, skipped_cards = self._skippedStraightCards()
                if skipped_card_count > 0:
                        return self.returnData(False)
                card_values = [card.value for card in self.cards]
                if 'ace' in card_values and '5' in card_values:
                        return True, '5'
                else: return self.returnData(True)
        
        def _skippedStraightCards(self):
                ''' a helper function that determines which cards are needed to make a straight '''
                # because aces can wrap for straights, add a new '1' card to represent a low ace
                orig_all_values = ['1'] + Card().all_values
                card_indecies = []
                cards = [card.value for card in self.cards]
                ace_added = False
                # if we have an ace in our hand, add a placeholder 1 card so that bot low and high straights are checked
                if 'ace' in cards: 
                        cards.append('1')
                        ace_added = True
                for card in cards:
                        all_values = orig_all_values[:]
                        index = all_values.index(card)
                        card_indecies.append(index)
                card_indecies = sorted(card_indecies)
                skipped_cards = []
                indecies_between = []
                total_skipped_card_count = 0
                for i, index in enumerate(card_indecies):
                        if i == len(card_indecies) - 1: break
                        indecies_between.append(card_indecies[i+1] - index-1)
                # if we have a pair we can't make a straight, return 5 so that it will
                # always be larger than the number of cards left               
                if -1 in indecies_between: return 5, skipped_cards
                # if an ace was added, remove the ace furthest from its neighbor 
                if ace_added:
                        max_difference = max(indecies_between[0], indecies_between[-1])
                        ace_index = indecies_between.index(max_difference)
                        card_indecies.remove(card_indecies[ace_index])
                        cards.remove(cards[ace_index])
                        indecies_between.remove(max_difference)

                total_skipped_card_count = sum(indecies_between)
                for i, index_difference in enumerate(indecies_between):
                        for j in range(index_difference):
                                skipped_cards.append(orig_all_values[card_indecies[i]+j+1])

                return total_skipped_card_count, skipped_cards
                                
                
        def makeStraight(self, suits = None):
                if suits is None: suits = Card().all_suits
                isstraight, _ = self.isStraight()
                if isstraight: return True
                possible_cards = []
                all_values = ['ace'] + Card().all_values
                skipped_card_count, skipped_values = self._skippedStraightCards()
                
                if skipped_card_count > self.cards_left: return False
                if '1' in skipped_values: 
                        skipped_values.append('ace')
                skipped_cards = []
                for suit in suits:
                        for card_value in skipped_values:
                                skipped_cards.append("{}_{}".format(card_value, suit))
                remaining_skipped_cards = [card for card in self.remaining_cards if card.name in skipped_cards]
                # if it's an inside straight return just those cards
                if skipped_card_count == self.cards_left:
                        return remaining_skipped_cards
                possible_min_cards = []
                possible_max_cards = []
                card_values = [card.get_numerical_value(self.low_ace) for card in self.cards]
                # remove the inside cards to see how many outside straight cards can be added
                cards_available = self.cards_left - skipped_card_count
                min_card = min(card_values)
                max_card = max(card_values)+1

                min_bottom_limit = max(min_card - cards_available, 0)
                max_top_limit = min(max_card + cards_available, len(all_values))
                possible_min_cards = [card for card in self.remaining_cards if(card.value in all_values[min_bottom_limit:min_card] and card.suit in suits)]
                possible_max_cards = [card for card in self.remaining_cards if(card.value in all_values[max_card:max_top_limit] and card.suit in suits) ]
                possible_cards = possible_min_cards + possible_max_cards + remaining_skipped_cards
                return possible_cards
                         

        def isFullHouse(self):
                if not self.complete(): return self.returnData(False)
                has_set, set_value = self.isSet()
                if not has_set: return False, set_value
                cards_without_set = [card for card in self.cards if card.value!= set_value]
                has_pair, pair_value = self.isPair(cards_without_set)
                if not has_pair: return False, set_value
                return True, set_value

                                
        def makeFullHouse(self):
                is_fh, _ = self.isFullHouse()
                if is_fh: return True
                is_set, set_value = self.isSet()
                # if we don't already have a set we need two pair
                if not is_set:
                        have_pair, pair1 = self.isPair()
                        if have_pair:
                                needed_cards_1 = []
                                needed_cards_2 = []
                                not_pair1 = [card for card in self.cards if card.value != pair1]
                                needed_cards_1 = self.makeSet(not_pair1)
                                if not needed_cards_1: return needed_cards_1
                                is_twopair, pair2 = self.isTwoPair()
                                if is_twopair:
                                        needed_cards_2 = self.makeSet([card for card in self.cards if card.value == pair1])
                                return needed_cards_1 + needed_cards_2
                        if self.cards_left == 3:
                                value1 = self.cards[0].value
                                value2 = self.cards[1].value
                                return [card for card in self.remaining_cards if card.value == value1 or card.value == value2]
                        return False
                                
                # return the cards needed to match the non-set cards
                new_cards = [card for card in self.cards if card.value != set_value]
                return self.makePair(new_cards)
        
        def isStraightFlush(self):
                if not self.complete(): return self.returnData(False)
                has_straight, _ = self.isStraight()
                has_flush, _ = self.isFlush()
                return self.returnData(has_straight and has_flush)

        def makeStraightFlush(self):
                is_sf, _ = self.isStraightFlush()
                if is_sf: return True
                flush_cards = self.makeFlush()
                if not flush_cards: return False
                straight_cards = self.makeStraight([self.cards[0].suit])
                if not straight_cards: return False
                return [card for card in self.remaining_cards if card in straight_cards]

        def EvaluateHand(self):
                ''' a function that returns the best current hand and the high card for that hand '''
                for hand, function in self.hand_order:
                        got_hand, high_card = function()
                        if got_hand: return hand, high_card

        
        def GetBestPossibleHand(self):
                ''' a function to return the best possible hand that can be generated from a set of cards
                all possible hand functions return a list of needed cards if the hand can be made, a boolean True if
                the hand requirements are already met, and a boolean false if it is impossible to make the hand'''
                for i, (hand, function) in enumerate(self.possible_hand_functions):
                        cards_needed = function()
                        if cards_needed: return hand, cards_needed
                        
        def complete(self):
                ''' check if any more cards will be drawn '''
                return self.cards_left == 0 
                
                
class Card():
        def __init__(self, card_name=None):
                
                self.all_suits = ["clubs", "spades", "hearts", "diamonds"]
                self.all_values = ['2','3','4','5','6','7','8','9','10',"jack","queen","king","ace"]
                self.name = card_name
                if card_name is None:
                        self.value=None
                        self.suit = None
                else:
                        self.value = card_name.split("_")[0]
                        self.suit = card_name.split("_")[1]

        def get_numerical_value(self, low_ace = False):
                all_values = ['1'] + self.all_values
                if low_ace and self.value == 'ace':
                        return all_values.index('1')
                return all_values.index(self.value)

        def all_cards(self):
                all_cards = []
                for value in self.all_values:
                        for suit in self.all_suits:
                                all_cards.append(Card("{}_{}".format(value, suit)))
                return all_cards

class OutsCounter():
        def __init__(self, pocket_cards, table_cards, state):
		self.pocket_cards = [Card(card) for card in pocket_cards]
		self.table_cards = [Card(card) for card in table_cards]
                self.state = state 
                self.states = ['Pre-Flop', 'Flop', 'Turn', 'River']
                self.outs_multiplier = 4 if state in self.states[:2] else 2
                self.visible_cards = self.pocket_cards + self.table_cards
                all_hands = itertools.combinations(self.visible_cards, 5)
                
                #require at least 2 known cards per hand (don't try to determine outs for sets of none)
                self.hands = []
                for hand in all_hands:
                        hand_name = [c.name for c in hand]
                        if hand_name.count(None)<=3:
                                self.hands.append(hand)

        def SortAndGroupHands(self, all_hands, score_type):
                # for any hands with duplicate scores, only add the ones with the best odds
                grouped_hands = [] 
                sorted_hands = sorted(all_hands, key= lambda hand_report: (hand_report[score_type], 100-hand_report['odds']))
                for hand_report in sorted_hands:
                        if hand_report[score_type] not in [group[score_type] for group in grouped_hands]: grouped_hands.append(hand_report)
                return grouped_hands

        def getHandScore(self, hand, best_hand, high_card):
                hand_order_list = [x for x,y in hand.hand_order]
                #cards = [c for c in hand.hand if c!= None]
                all_values = Card().all_values
                if hand.low_ace: all_values = ['ace'] + all_values[:-1]
                else: all_values = ['None'] + all_values
                #high_card_value = max([c.get_numerical_value(hand.low_ace) for c in cards])
                high_card_value = all_values.index(high_card)
                # in order for it to sort by lowest to highest, reverse the ordering.
                high_card_score = 14 - high_card_value
                hand_score = hand_order_list.index(best_hand)
                return (hand_score, high_card_score)

        def createHandReport(self, hand, needed_cards, best_hand, current_hand, possible_high_card, current_high_card):
                ''' a function to put all of the necessary information in an easily accessible dictonary '''
                deck_values = ['ace'] + Card().all_values
                hand_order_list = [x for x,y in hand.hand_order]
                hand_report = {}
                card_names = hand.card_names
                hand_report['possible'] = [c.name for c in needed_cards]
                hand_report['cards'] = card_names
               
                # determine the tiebreaker score 
 
                possible_full_hand = HandEvaluator(needed_cards + [c for c in hand.hand if c != None])
                possible_hand_score = self.getHandScore(possible_full_hand, best_hand, possible_high_card) 
                current_hand_score = self.getHandScore(hand, current_hand, current_high_card)  
        
                hand_report['score'] = possible_hand_score
                hand_report['current_hand'] = current_hand
                hand_report['tiebreaker'] = deck_values[-hand_report['score'][1]]
                hand_report['hand_classification'] = " {} with {} high".format(best_hand, hand_report['tiebreaker'])
                hand_report['odds'] = self.outs_multiplier*len(needed_cards) if len(needed_cards) else 100 
                hand_report['current_hand_score']= current_hand_score
                
                #print " {} gives a possible hand of {} with {} high. A possible score of {} ".format(hand_report['cards'], hand_report['possible'], hand_report['tiebreaker'], hand_report['score'])
        
                return hand_report 


        def GenerateReport(self):
                all_hands = []
                if not self.hands: return
                for h in self.hands:
                        leftover_cards = [card for card in self.visible_cards if(card.name is not None and card.name not in [c.name for c in h])]
                        hand = HandEvaluator(h)
                        hand.removeKnownCards(leftover_cards)
                        
                        best_hand, possible = hand.GetBestPossibleHand()

                        # evaluate the hand that is already complete
                        current_hand, current_high_card = hand.EvaluateHand()

                        # create all combinations of # of cards left sets the out cards and checks to see
                        # which ones make the desired hand. Add all hands that fit those requirements
                        
                        # possible will be either a True boolean (if the hand is already complete) or a list of needed cards to make the hand
                        if isinstance(possible, list):
                                if len(possible) < hand.cards_left: all_outs = [possible]
                                else: all_outs = itertools.combinations(possible, hand.cards_left)
                                all_scores = []
                                all_outs_cards = []
                                all_high_cards = []
                                for outs_group in all_outs:
                                        test_hand = HandEvaluator([c for c in h if c.name !=None] + list(outs_group))
                                        test_hand_type, high_card  = test_hand.EvaluateHand()
                                        # check to see that this combination of cards makes the correct hand
                                        if test_hand_type == best_hand:
                                                hand_score = self.getHandScore(test_hand, best_hand, high_card)
                                                if hand_score not in all_scores:
                                                        all_scores.append(hand_score)
                                                        all_outs_cards.append(list(outs_group))
                                                        all_high_cards.append(high_card)
                                                else:
                                                        index = all_scores.index(hand_score)
                                                        for out_card in outs_group:
                                                                if out_card not in all_outs_cards[index]: 
                                                                        all_outs_cards[index] += [out_card]

                                for outs, high_card in zip(all_outs_cards, all_high_cards):
                                        hand_report = self.createHandReport(hand, outs, best_hand, current_hand, high_card, current_high_card)
                                        all_hands.append(hand_report)
                                
                        else:
                                hand_report = self.createHandReport(hand, [], best_hand, current_hand, current_high_card, current_high_card)
                                all_hands.append(hand_report)
              
                # sort all the possible hands by the score and then the tiebreaker 
                sorted_current_hand = self.SortAndGroupHands(all_hands, 'current_hand_score')
                
                print '----------------{}--------------------'.format(self.state)
                print "Visible Cards : {}".format([card.name for card in self.visible_cards])
                print "Current Hand {} with {} high using {}".format(sorted_current_hand[0]['current_hand'], sorted_current_hand[0]['tiebreaker'], sorted_current_hand[0]['cards'])
                #Only calculate possible hands if we still have cards to come
                if [c.name for c in self.visible_cards].count(None):
                    sorted_hands = self.SortAndGroupHands(all_hands, 'score')
                    list_length = min(5, len(sorted_hands))
                    print "Top {} Possible hands".format(list_length)
                    for i in range(list_length):
                            print "{}) Type: {}\n    Cards Used: {} \n    Cards Needed: {}\n   Odds: {}%".format( i+1, sorted_hands[i]['hand_classification'], sorted_hands[i]['cards'], sorted_hands[i]['possible'], sorted_hands[i]['odds'])
                          

        
# Testing                      
if __name__ == '__main__':
        states = [('Pre-Flop', 2), ('Flop', 3), ('Turn',1), ('River', 1)]
        all_cards = []
        all_cards.append('3_clubs')
        all_cards.append('3_spades')
        all_cards.append('jack_clubs')
        all_cards.append('king_hearts')
        all_cards.append('ace_diamonds')
        all_cards.append('jack_spades')
        all_cards.append('jack_diamonds')

        #test_hand = [Card('2_hearts'), Card('4_spades'), Card('3_hearts'), Card('ace_hearts'), Card(None)]
        #print [card.name for card in test_hand]
        #hand = HandEvaluator(test_hand)
        #print hand.GetBestPossibleHand()
        card_count = 0
        for (state, new_cards) in states:
                cards = [None, None, None, None, None, None, None]
                card_count +=  new_cards
                cards[:card_count] = all_cards[:card_count]
        
                oc = OutsCounter(cards[:2], cards[2:], state)
                oc.GenerateReport()

                #print '\n OPPONENT REPORT'
                #opponent = OutsCounter([None, None], cards[2:], state)
                #opponent.GenerateReport()
        
