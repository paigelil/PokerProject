import cv2
import os
import sys
import time

file_path = os.path.dirname(os.path.abspath(__file__))
screenshot_dir = os.path.join(file_path,"..","screenshots")
used_screenshot_dir = os.path.join(file_path, "..", "training_screenshots")
template_directory = os.path.join(file_path, "..", "card_images", "table_cards")

template_width = 18
template_height = 40
tm_buffer = 5

first_card_position = (325,233)
card_spacing = 51

all_cards = []
suits=["spades", "clubs", "hearts", "diamonds"]
values = ["ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king"]
for suit in suits:
	for value in values:
		all_cards.append("{}_{}.png".format(value,suit))


def check_missing():
	saved_cards = os.listdir(template_directory)
	missing_cards = []
	for card in all_cards:
		if card not in saved_cards:
			missing_cards.append(card)

	print "Missing the following cards", missing_cards
	if len(missing_cards): return True
	return False

def load_templates():
	templates = []
	for template in os.listdir(template_directory):
		temp_image = cv2.imread(os.path.join(template_directory,template))
		if temp_image is not None: templates.append(temp_image)
	return templates

def match_templates(cropped_image, templates):
	for template in templates:
		match_result = cv2.matchTemplate(cropped_image, template, cv2.TM_CCOEFF_NORMED)
		mini, maxi, min_loc, max_loc = cv2.minMaxLoc(match_result)
		if maxi > 0.95: return True
	return False
		
def find_cards():
	for screenshot in os.listdir(screenshot_dir):
		print "Loading {}".format(screenshot)
		img = cv2.imread(os.path.join(screenshot_dir, screenshot))
		if img is None: continue
		drawing_img = img.copy()
		templates = load_templates()
		for card_index in range(5):
			cv2.rectangle(drawing_img, (first_card_position[0]+card_index*card_spacing, first_card_position[1]), (first_card_position[0]+template_width + card_index*card_spacing, first_card_position[1]+template_height), [100,100,100],1)
			cv2.imshow("drawing image", drawing_img)
			cropped_card = img[first_card_position[1]:first_card_position[1]+template_height, first_card_position[0]+card_index*card_spacing:first_card_position[0]+template_width+card_index*card_spacing]
			already_matched = match_templates(img[first_card_position[1]-tm_buffer:first_card_position[1]+tm_buffer+template_height, first_card_position[0]-tm_buffer+card_index*card_spacing:first_card_position[0]+tm_buffer+template_width+card_index*card_spacing]
 , templates)
			print "already matched is {}".format(already_matched)
			if already_matched: continue
			cv2.imshow("Found Card", cropped_card)
			cv2.waitKey(1)
			card_name = raw_input("Which card is this?: ")
			if card_name != "":
				cv2.imwrite(os.path.join(template_directory, "{}.png".format(card_name)),cropped_card)
		cv2.imwrite(os.path.join(used_screenshot_dir, screenshot), img)
		os.remove(os.path.join(screenshot_dir, screenshot))
still_missing = True
while still_missing:
	find_cards()
	still_missing = check_missing()
	time.sleep(10)

